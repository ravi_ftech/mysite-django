from django.http import HttpResponse
from django.template import RequestContext, loader
from .models import Question, Choice

def index(request):
	latest_question_list = Question.objects.order_by('-pub_date')[:5]
	template = loader.get_template('myapp/index.html')
	context = RequestContext(request, {
		'latest_question_list': latest_question_list,
		})
	return HttpResponse(template.render(context))

def detail(request, question_id):
	polls = Choice.objects.filter(question__id = question_id)
	poll_question = Question.objects.get(id = question_id)
	template = loader.get_template('myapp/polls.html')
	context = RequestContext(request, {
		'polls': polls,
		'poll_question': poll_question,
		})
	return HttpResponse(template.render(context))
	#return HttpResponse("Yor're looking at question %s." % question_id)

def results(request, question_id):
	response = "You're looking at results of question %s."
	return HttpResponse(response % question_id)

def vote(request, question_id):
	return HttpResponse("You're voting on the question %s." % question_id)