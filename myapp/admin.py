import datetime
from django.utils import timezone
from django.contrib import admin

from .models import Question, Choice

class ChoiceInline(admin.TabularInline):
	model = Choice
	extra = 3

class QuestionAdmin(admin.ModelAdmin):

	list_display = ('question_text', 'pub_date', 'was_published_recently')

	list_filter = ['pub_date']

	search_fields = ['question_text']

	fieldsets = [
		(None, 					{'fields': ['question_text']}),
		('Date information', 	{'fields': ['pub_date']}),
	]
	inlines = [ChoiceInline]

	def was_published_recently(self, arg2):
		return arg2.pub_date >= timezone.now() - datetime.timedelta(days=1)

	was_published_recently.admin_order_field = 'pub_date'
	was_published_recently.boolean = True
	was_published_recently.short_description = 'Published recently?'

admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice)